# git-wtf

A Docker image to run `git-wtf`.

Because you may not want to install Ruby locally.

## Usage

`WORKINGDIR` is `/src`.

From your shell:

```shell
docker run -it --rm \
    --name git-wtf \
    -v "$PWD":/src \
    --read-only \
    letompouce/git-wtf
```

The `git-wtf` bash script is a convenient wrapper to be placed in your
`PATH`, such as `/usr/local/bin/git-wtf`.

Then just use it normally:

```shell
git-wtf --help
git-wtf --long
```

## Upstream

* `git-wtf.rb` from <https://github.com/DanielVartanov/willgit>
* This docker image at <https://gitlab.com/l3tompouce/docker/git-wtf>
