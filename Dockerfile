FROM ruby:3-alpine

COPY git-wtf.rb /usr/local/bin/git-wtf

# hadolint ignore=DL3018
RUN apk add --no-cache \
        git

WORKDIR /src
ENTRYPOINT [ "/usr/local/bin/git-wtf" ]
